// SPDX-License-Identifier: CC0

package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

// Try: nyt -words | sort | dups FS='\t'

var wordsOnly = flag.Bool("words", true, "print words and masks, then exit")

var box Box = makeBox("hifmeltubdra")

type Box struct {
	letters string
	letterIndices [26]int  // one-biased, convert letter to index
}

type Chain struct {
	words []string
	mask uint
}

func chainFromString(raw string) (Chain, bool) {
	bytes := []byte(raw)
	mask := uint(0)
	slow := 0
	side := -1
	for fast := 0; fast < len(bytes); fast++ {
		// Skip any non-alphabetic rune.
		r := cleanRune(rune(bytes[fast]))
		if r == '\x00' {
			continue
		}
		// Then make sure it's a valid letter.
		i := box.letterIndices[r-'a']  // biased +1 (!)
		if i == 0 {
			return Chain{}, false
		}
		// Then make sure it's not on the same side as the
		// previous letter.
		if (i-1)/3 == side {
			return Chain{}, false
		}
		side = (i-1)/3
		// Passed all checks, update mask and write pointer.
		mask |= 1 << i
		slow++
	}
	cleanedString := string(bytes[:slow])
	return Chain{ words: []string{cleanedString}, mask: mask }, true
}

func cleanRune(r rune) rune {
	if 'a' <= r && r <= 'z' {
		return r
	} else if 'A' <= r && r <= 'Z' {
		return r - 'A' + 'a'
	} else {
		return '\x00'
	}
}

func main() {
	flag.Parse()

	chains, err := readChains("3of6game.txt")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if *wordsOnly {
		for _, c := range chains {
			fmt.Printf("%v\t%v\n", stringFromMask(c.mask), c.words[0])
		}
		os.Exit(0)
	}
}

func makeBox(letters string) Box {
	box := Box{letters: letters}
	for i, r := range letters {
		r = cleanRune(r)
		if r != '\x00' {
			box.letterIndices[r-'a'] = i+1
		}
	}
	return box
}

func readChains(dictfile string) ([]Chain, error) {
	fd, err := os.Open(dictfile)
	if err != nil {
		return nil, err
	}
	defer fd.Close()

	var chains []Chain
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		if c, b := chainFromString(scanner.Text()); b {
			chains = append(chains, c)
		}
	}
	return chains, scanner.Err()
}

func stringFromMask(mask uint) string {
	runes := make([]rune, len(box.letters))
	for i, ch := range box.letters {
		if mask & (1 << box.letterIndices[ch-'a']) != 0 {
			runes[i] = ch
		} else {
			runes[i] = ' '
		}
	}
	return string(runes)
}
