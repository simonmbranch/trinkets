/* SPDX-License-Identifier: CC0 */

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>

const char *names[33] = {
	"NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL",
	"BS",  "HT",  "LF",  "VT",  "FF",  "CR",  "SO",  "SI",
	"DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB",
	"CAN", "EM",  "SUB", "ESC", "FS",  "GS",  "RS",  "US",
	"SPC"
};

const char *usage = "usage: ascii [-01248bdmoux]";

char info[4];
unsigned int columns = 0;
unsigned int count = 128;
unsigned int ninfo = 0;
unsigned int spaces;
unsigned int width = 6;

static void showinfo(unsigned char);

int
main(int argc, char **argv)
{
	(void)argc;
	for (char **arg = argv + 1; *arg; arg++) {
		for (char *ch = *arg; *ch; ch++) {
			switch (*ch) {
			case '-':
				break;
			case '0':
			case '1':
			case '2':
			case '4':
			case '8':
				columns = *ch - '0';
				break;
			case 'u':
				count = 32;
				break;
			case 'b':
			case 'd':
			case 'o':
			case 'x':
				if (ninfo == 4)
					break;
				info[ninfo++] = *ch;
				if (*ch == 'b')
					width += 9;
				else if (*ch == 'x')
					width += 3;
				else
					width += 4;
				break;
			default:
				fprintf(stderr, "%s\n", usage);
				return 1;
			}
		}
	}

	if (columns == 0) {
		if (width <= 9)
			columns = 8;
		else if (width <= 18)
			columns = 4;
		else
			columns = 2;
	}

	for (unsigned int row = 0; row < count/columns; row++) {
		spaces = 0;
		for (unsigned char c = row; c < count; c += count/columns) {
			for (unsigned int i = 0; i < spaces; i++)
				printf(" ");
			showinfo(c);
		}
		printf("\n");
	}
}

static void
showinfo(unsigned char c)
{
	for (unsigned int i = 0; i < ninfo; i++) {
		switch (info[i]) {
		case 'b':
			for (unsigned int bit = 0x80; bit; bit >>= 1)
				printf("%c", (c & bit) ? '1' : '0');
			break;
		case 'o':
			printf("%03hho", c);
			break;
		case 'd':
			printf("%03hhd", c);
			break;
		case 'x':
			printf("%02hhX", c);
			break;
		}
		printf(" ");
	}
	if (c < 33)
		spaces = 6 - printf("%s", names[c]);
	else if (c == 127)
		spaces = 6 - printf("DEL");
	else
		spaces = 6 - printf("%c", c);
}
