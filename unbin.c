/* Swarthmore College sent me an email in binary code.
 * I guess it was effective. */
#include <stdio.h>
int main(void)
{
	static int c, n, i;
	for (;;) switch ((c = getchar())) {
	case EOF:
		putchar('\n');
		return 0;
	case '0':
		n *= 2;
		i++;
		goto process;
	case '1':
		n = n*2 + 1;
		i++;
	process:
		if (i == 8) { putchar(n); n = i = 0; }
		break;
	default:
		putchar(c);
		break;
	}
}
