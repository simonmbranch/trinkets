#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <time.h>
 
#define	timespecsub(tsp, usp, vsp)					\
	do {								\
		(vsp)->tv_sec = (tsp)->tv_sec - (usp)->tv_sec;		\
		(vsp)->tv_nsec = (tsp)->tv_nsec - (usp)->tv_nsec;	\
		if ((vsp)->tv_nsec < 0) {				\
			(vsp)->tv_sec--;				\
			(vsp)->tv_nsec += 1000000000L;			\
		}							\
	} while (0)
#define	timespecadd(tsp, usp, vsp)					\
	do {								\
		(vsp)->tv_sec = (tsp)->tv_sec + (usp)->tv_sec;		\
		(vsp)->tv_nsec = (tsp)->tv_nsec + (usp)->tv_nsec;	\
		if ((vsp)->tv_nsec >= 1000000000L) {			\
			(vsp)->tv_sec++;				\
			(vsp)->tv_nsec -= 1000000000L;			\
		}							\
	} while (0)

int main(void) {
	struct timespec start;
	clock_gettime(CLOCK_MONOTONIC, &start);
	struct timespec desired;
	clock_gettime(CLOCK_MONOTONIC, &desired);
	struct timespec dt;
	dt.tv_sec = 0;
	dt.tv_nsec = 1000 * 1000;

	for (int i = 0; i < 750; i++) {
		timespecadd(&desired, &dt, &desired);
		struct timespec now;
		clock_gettime(CLOCK_MONOTONIC, &now);
		struct timespec waittime;
		timespecsub(&desired, &now, &waittime);
		nanosleep(&waittime, NULL);
		printf("%d\n", i);
	}

	struct timespec end;
	clock_gettime(CLOCK_MONOTONIC, &end);

	struct timespec elapsed;
	timespecsub(&end, &start, &elapsed);
	printf("%ld milliseconds\n", elapsed.tv_nsec / 1000 / 1000);
}
