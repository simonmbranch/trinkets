#!/usr/bin/env perl
# SPDX-License-Identifier: CC0

use strict;
use warnings;
use autodie;
use feature qw(signatures);
use Carp qw(confess);
use Encode qw(is_utf8 encode decode);

my $linesperpage = 80;

my $offset = 0;
my $streamoffset = 0;
my $linecount = 0;
my @xref;
my @pages;
my $obj_unused = ++$#xref;
my $obj_catalog = ++$#xref;
my $obj_pagetree = ++$#xref;

sub emit(@s) { local $_ = join("", @s)."\n"; $offset += length; print; }
sub startobj($n = @xref) { $xref[$n] = $offset; emit "$n 0 obj"; $#xref }
sub endobj() { emit "endobj\n"; }

sub startpage() {
	# 3.6.2
	$linecount == 0 or confess "startpage() called with nonzero \$linecount";
	push @pages, startobj;
	emit "<< /Type /Page /Parent $obj_pagetree 0 R ",
		"/Contents ", $#xref+1, " 0 R >>";
	endobj;
	startobj;
	emit sprintf "<< /Length %d 0 R >>\nstream", $#xref+1;
	$streamoffset = $offset;
	# 3.1
}

sub endpage() {
	my $length = $offset - $streamoffset;
	emit "endstream";
	endobj;
	startobj; emit $length; endobj;
	$linecount = 0;
}

sub startdocument() {
	# 3.4.1
	emit "%PDF-1.3";
	emit "% scribit-generated binary PDF \xFF\xFF\xFF\xFF\x00";
	# 3.6.1
	startobj $obj_catalog;
	emit "<< /Type /Catalog /Pages $obj_pagetree 0 R >>";
	endobj;
}

sub emitline($s) {
	startpage if $linecount == 0;
	emit "BT /F1 18 Tf 40 50 Td (Hello, world!) Tj ET";
	$linecount++;
	endpage if $linecount == $linesperpage;
}

sub enddocument() {
	# 3.6.2
	startobj $obj_pagetree;
	emit "<< /Type /Pages";
	my @pagerefs = map "$_ 0 R", @pages;
	emit "/Kids [@pagerefs] /Count ", scalar(@pages);
	emit "/Resources <<";
	emit "/Font << /F1 << /Type /Font /Subtype /Type1 /BaseFont /Helvetica >> >>";
	emit "/ProcSet [/PDF /Text] >>"; # 3.7.2
	emit "/MediaBox [0 0 612 792] >>";
	endobj;

	# 3.4.3
	my $xrefoffset = $offset;
	my $objcount = @xref;
	emit "xref\n0 $objcount";
	emit "0000000000 65535 f ";  # unused entry
	for my $i (1..$#xref) {
		emit sprintf "%010d 00000 n ", $xref[$i];
	}
	# File trailer (3.4.4).
	emit "trailer << /Size $objcount /Root $obj_catalog 0 R >>";
	emit "startxref\n$xrefoffset\n%%EOF";
}

startdocument;
emitline $_ while <STDIN>;
endpage if $linecount;
enddocument;
