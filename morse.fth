\ SPDX-License-Identifier: CC0

\ Morse code encoder.  Only spaces and alphabetic input are processed
\ correctly.  Capital letters supress the normal intra-letter pause,
\ so that you can enter prosigns:
\
\   simon ... .. -- --- -.
\   SOS ...---...
\   INT ..-.-

CREATE codetab
    HEX HERE 20 ERASE
    2 BASE !
    0 C,
    10011111 C,  00010111 C,  \ A  .-    B  -...
    01010111 C,  00101111 C,  \ C  -.-.  D  -..
    00111111 C,  01000111 C,  \ E  .     F  ..-.
    01101111 C,  00000111 C,  \ G  --.   H  ....
    00011111 C,  11100111 C,  \ I  ..    J  .---
    10101111 C,  00100111 C,  \ K  -.-   L  .-..
    11011111 C,  01011111 C,  \ M  --    N  -.
    11101111 C,  01100111 C,  \ O  ---   P  .--.
    10110111 C,  01001111 C,  \ Q  --.-  R  .-.
    00001111 C,  10111111 C,  \ S  ...   T  -
    10001111 C,  10000111 C,  \ U  ..-   V  ...-
    11001111 C,  10010111 C,  \ W  .--   X  -..-
    11010111 C,  00110111 C,  \ Y  -.--  Z  --..
    HEX codetab 20 + HERE - ALLOT

CREATE silence 0 ,

: reset ( -- ) 0 silence ! ;
: flush ( -- ) silence @ 0 ?DO [CHAR] . EMIT LOOP reset ;
: pause ( u -- ) silence @ MAX silence ! ;
: beep  ( u -- ) flush 0 ?DO [CHAR] # EMIT LOOP ;
: get-bit ( u -- u b ) DUP 2/ SWAP 1 AND ;
: letter>code ( c -- u ) 40 MAX  1F AND  codetab + C@ ;
: 1bits ( u -- u ) 1 SWAP LSHIFT 1- ;

: decode ( c -- c u )
    8 >R BEGIN
        R> 1- >R
        get-bit
    0= UNTIL R> ;

: encode ( c u -- c )
    8 SWAP - TUCK LSHIFT SWAP 1- 1bits OR ;

: morse-emit-decoded ( c u -- )
    0 ?DO
        get-bit IF 3 ELSE 1 THEN beep
        1 pause
    LOOP DROP ;

: morse-emit ( c -- )
    letter>code
    DUP IF
        decode morse-emit-decoded
    ELSE
        DROP  7 pause
    THEN ;

: morse-type ( addr u -- )
    reset
    OVER + SWAP ?DO
        I C@ DUP morse-emit
        20 AND 20 =  IF 3 pause THEN
    LOOP ;

: morse"   [CHAR] " PARSE morse-type ;

: morse-loop ( -- )
    BEGIN
        PAD 80 ACCEPT
    DUP WHILE
        PAD SWAP morse-type CR
    REPEAT DROP ;
