// SPDX-License-Identifier: CC0

package main

import (
	"fmt"
	"flag"
	"math/rand/v2"
)

type Player struct {
	buncos     int
	minibuncos int
}

type Pair struct {
	players [2]Player
	points  int
	next    int
}

type Table struct {
	pairs [2]Pair
	next  int
}

var talkative bool

func roll(player *Player, bunco int) int {
	d1 := rand.IntN(6)
	d2 := rand.IntN(6)
	d3 := rand.IntN(6)
	points := 0
	if d1 == bunco { points++; }
	if d2 == bunco { points++; }
	if d3 == bunco { points++; }
	if talkative && points > 0 {
		fmt.Printf("%v, %v, %v: that's ", d1, d2, d3)
	}
	if points == 3 {
		player.buncos++
		if talkative {
			fmt.Println("a BUNCO!!!  21 points!")
		}
		return 21
	} else if d1 == d2 && d2 == d3 {
		player.minibuncos++
		if talkative {
			fmt.Println("a Mini Bunco!  5 points!")
		}
		return 5
	} else {
		if talkative && points > 0 {
			if points == 1 {
				fmt.Printf("%v point\n", points)
			} else {
				fmt.Printf("%v points\n", points)
			}
		}
		return points
	}

}

func play(table *Table, bunco int, cutoff int) bool {
	var pair *Pair = &table.pairs[table.next]
	var player *Player = &pair.players[pair.next]
	for {
		points := roll(player, bunco)
		pair.points += points
		if talkative && points > 0 {
			fmt.Printf("[score: %v - %v] ", table.pairs[0].points, table.pairs[1].points)
		}
		if points == 0 {
			break
		}
		if pair.points >= cutoff {
			break
		}
	}
	pair.next = (pair.next+1) % 2
	table.next = (table.next+1) % 2
	return pair.points >= cutoff
}

func round(table *Table, bunco int) {
	for i, _ := range table.pairs {
		table.pairs[i].points = 0
		table.pairs[i].next = 0
	}
	table.next = 0
	for !play(table, bunco, 21) {
		// Do nothing
	}
}

func night(threshold int, rounds int) int {
	var table = Table{}
	for i := 1; i <= rounds; i++ {
		round(&table, 1)
	}
	lucky := 0
	for _, pair := range table.pairs {
		for _, player := range pair.players {
			if player.buncos >= threshold {
				lucky++
			}
		}
	}
	return lucky
}

func main() {
	var threshold int
	var nights int
	var rounds int

	flag.IntVar(&threshold, "threshold", 3, "number of buncos to be considered lucky")
	flag.IntVar(&nights, "nights", 10000, "number of bunco nights to simulate")
	flag.IntVar(&rounds, "rounds", 10, "number of rounds per night")
	flag.BoolVar(&talkative, "talkative", false, "whether to narrate games")
	flag.Parse()

	lucky := 0
	for i := 1; i <= nights; i++ {
		lucky += night(threshold, rounds)
	}
	fmt.Printf("over %v nights, only %v people got at least %v buncos, with chance %v\n",
			nights, lucky, threshold,
			float32(lucky) / float32(nights) / float32(4))
}
