#!/bin/sh
# SPDX-License-Identifier: CC0

# This script replicates NetHack's messages for a full moon (+1 luck),
# new moon, and Friday the 13th (-1 luck).  The moon phases last three
# days and approximate the moon's actual position.
# 
# The calculations are from NetHack 3.6.7, files allmain.c:47 and
# hacklib.c:1105.
#
# The months where Friday the 13th is a full moon (so that the +1 and -1
# cancel each other out), from 1995 to 2040, are:
#     Feb 1998   Jun 2003   Jan 2017   Sep 2030
#     Mar 1998   Mar 2009   Sep 2019   May 2033
#     Oct 2000   Jun 2014   Jun 2025   Aug 2038

phase() {
	# Note that in the original algorithm, tm_year is years since
	# 1900, and tm_yday ranges from 0 to 365.  Here, $1 is years
	# A.D. and $2 is day of the year from 1 to 366.
	dc -e "$1 1900- 19%1+dsG 11*18+30% [1+]sA[lG11<A]sB d25=B d24=A
	$2 1- +6*11+177%22/8%p"
}

if [ $# -eq 2 ]; then
	phase "$@"
	exit
fi

set -- 
case $(phase $(date +'%Y %j')) in
	(4) echo 'You are lucky!  Full moon tonight.';;
	(0) echo 'Be careful!  New moon tonight.';;
esac

if [ "$(date +'%u %d')" = "5 13" ]; then
	echo 'Watch out!  Bad things can happen on Friday the 13th.'
fi

# use strict;
# use warnings;
# use Time::Piece;
# 
# for my $year (1995..2040) {
#     for my $month (1..12) {
#         my $t = Time::Piece->strptime("$year $month 13", "%Y %m %d");
#         my $yday = $t->yday+1;
#         if ($t->wday == 6 && `moon $year $yday` =~ /4/) {
#             print $t->strftime("%b %Y"), "\n";
#         }
#     }
# }
