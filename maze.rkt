#lang racket

(require racket/draw noise)

(define width 31)
(define height 91)

(define (maze)  ; a vector representing the grid in row-major order
  (unless (and (odd? width) (odd? height))
    (error "dimensions of maze must be odd"))
  (let* ((pixels (* width height))
         (grid (make-vector pixels #f))
         (directions (list 1 -1 width (- width))))
    (define (random-odd hi)  ; an odd number x such that 1 <= x < hi
      (+ 1 (* 2 (random (truncate (/ hi 2))))))
    ;; We use the recursive backtracking algorithm.  All recursion is
    ;; iterative; the stack holds grid positions to backtrack to.
    ;; https://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
    ;; https://weblog.jamisbuck.org/2011/1/27/maze-generation-growing-tree-algorithm.html
    (let maze-loop ((stack (list (+ (random-odd width)
                                    (* width (random-odd height))))))
      (unless (null? stack)
        (let ((pos (car stack)))
          ;; This happens more than once per pos but it doesn't matter.
          (vector-set! grid pos #t)
          (let neighbor-loop ((ds (shuffle directions)))
            (if (null? ds)
                ;; Dead end.
                (maze-loop (cdr stack))
                ;; Otherwise check if this direction yields a good neighbor.
                (let ((wall (+ pos (car ds)))
                      (next-pos (+ pos (* 2 (car ds)))))
                  (if (or (< next-pos 0)       ; through the ceiling?
                          (>= next-pos pixels) ; through the floor?
                          (and (= 1 (car ds))  ; wrapped around to the right?
                               (< (modulo next-pos width) 2))
                          (and (= -1 (car ds)) ; wrapped around to the left?
                               (< (modulo pos width) 2))
                          (vector-ref grid next-pos)) ; already seen?
                      ;; Bad neighbor, try again.
                      (neighbor-loop (cdr ds))
                      ;; Good neighbor, carve a wall out and recurse on
                      ;; that square.
                      (begin
                        (vector-set! grid wall #t)
                        (maze-loop (cons next-pos stack))))))))))
    grid))

(define (srgb-gamma x)
  ;; The code from the Oklab website returns linear sRGB values.  This
  ;; means that they are perceived linearly by humans, however the rest
  ;; of the computer world works with regular sRGB values which relate
  ;; to the actual light.  The two values are related by a power.
  ;; https://en.wikipedia.org/wiki/SRGB#From_CIE_XYZ_to_sRGB
  (if (< x 0.0031308)
      (* 12.92 x)
      (- (* 1.055 (expt x (/ 2.4)))
         0.055)))

(define (rational->byte x)
  ;; A more sophisticated approach would be gamut mapping.  Many pixels
  ;; of the output must be clamped and so we may be losing quality.
  (cond ((> x 1) 255)
        ((< x 0) 0)
        (else (exact-truncate (* 256 (abs x))))))

(define (oklab->color L a b)
  (let ((l_ (+ L (* a 0.3963377774) (* b 0.2158037573)))
        (m_ (+ L (* a -0.1055613458) (* b -0.0638541728)))
        (s_ (+ L (* a -0.0894841775) (* b -1.2914855480))))
    (let ((l (* l_ l_ l_))
          (m (* m_ m_ m_))
          (s (* s_ s_ s_)))
      (let ((r (+ (* l 4.0767416621) (* m -3.3077115913) (* s 0.2309699292)))
            (g (+ (* l -1.2684380046) (* m 2.6097574011) (* s -0.3413193965)))
            (b (+ (* l -0.0041960863) (* m -0.7034186147) (* s 1.7076147010))))
        (make-color (rational->byte (srgb-gamma r))
                    (rational->byte (srgb-gamma g))
                    (rational->byte (srgb-gamma b)))))))

(define (oklch->color L C h)
  (let ((a (* C (cos (* 2 pi h))))
        (b (* C (sin (* 2 pi h)))))
    (oklab->color L a b)))

(define (color->hex c)
  (define (hex n)
    (~r n #:base 16 #:min-width 2 #:pad-string "0"))
  (format "#~a~a~a" (hex (send c red))
                    (hex (send c green))
                    (hex (send c blue))))

(define (color->ansi c)
  (printf "\033[38;2;~a;~a;~amHello\033[0m~n"
          (send c red)
          (send c green)
          (send c blue)))

(define (px n) (* n 10))

(define perlin-scale 9)

(define (choose-color x y wall?)
  (let* ((perlin-output (perlin (* perlin-scale (/ x width))
                                (* perlin-scale (/ y height)))))
    (if wall?
        "white"
        (oklch->color 0.9
                      (+ 0.2 (* 0.1 perlin-output))
                      (/ y height)))))

(define (draw-image)
  (let* ((bitmap (make-bitmap (px width) (px height)))
         (dc (new bitmap-dc% (bitmap bitmap))))
    (define (draw-pixel x y color)
      (send dc set-brush color 'solid)
      (send dc draw-rectangle (px x) (px y) (px 1) (px 1)))
    (send dc set-pen (new pen% (style 'transparent)))
    (send dc set-background "white")
    (let ((coordinates (for*/list ([y (in-range height)]
                                   [x (in-range width)])
                         (cons x y))))
      (for ([elem (maze)]
            [coordinate coordinates])
        (let ((x (car coordinate))
              (y (cdr coordinate)))
          (draw-pixel x y (choose-color x y elem)))))
    (send bitmap save-file "maze.png" 'png)))

;(draw-image)
