-- Trench 3.1 #11

import Text.Printf (printf)

deriv x y = (-1) * (2*x + cos x) / (3*y*y + 4*y)
initValue = (0,1)

step f' h (x,y) = (x+h, y+h*(f' x y))

skip :: Int -> [a] -> [a]
skip n [] = []
skip n (x:xs) = x : skip n (drop n xs)

h0_1     = take 11 $ skip 0  $ iterate (step deriv 0.1)     initValue
h0_05    = take 11 $ skip 1  $ iterate (step deriv 0.05)    initValue
h0_025   = take 11 $ skip 3  $ iterate (step deriv 0.025)   initValue
h0_0125  = take 11 $ skip 7  $ iterate (step deriv 0.0125)  initValue
h0_00625 = take 11 $ skip 15 $ iterate (step deriv 0.00625) initValue

printResults :: [(Double,Double)] -> IO ()
printResults results = mapM_ printResult results
  where printResult (x,y) = putStrLn $ printf "%.3f\t%.4g" x y
