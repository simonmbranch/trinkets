-- Trench 5.4 #21

import Data.List (intercalate)

y :: Double -> Double
y x = -1*exp (-4*x) + 3*exp x + x*exp (2*x)

main = writeFile "/tmp/udcoef.dat" . unlines . map showPoint $ points
  where
    showPoint (x,y) = (show x) <> " " <> (show y)
    points = zip xs (map y xs)
    xs = [-2,-1.99..2]
