# Trench 4.3 #15
# vim:set ft=gnuplot:
set terminal pngcairo enhanced
set output "/tmp/rk.png"
set xrange [0:1]
set yrange [-12:0]
set title "Fall through a resistive medium where F ∝ v^4"
set xlabel "time (s)"
set ylabel "velocity (m/s)"
set arrow from 0,-5.309 to 1,-5.309 \
	nohead lt 2 dashtype 2 lw 1.5
set label "v = -5.309" at 0,-5.309 offset character 0,0.4 \
	font "Arial,9"
unset key
plot for [i=0:*] "/tmp/rk.dat" index i using 1:2 with lines
