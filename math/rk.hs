-- Trench 4.3 #15

import Data.List (intercalate)

step f h (x,y) = (x+h, y + h*s/6)
  where s = k1 + 2*k2 + 2*k3 + k4
        k1 = f x y
        k2 = f (x+h/2) (y+h*k1/2)
        k3 = f (x+h/2) (y+h*k2/2)
        k4 = f (x+h) (y+h*k3)

v' t v = -9.81 + (v^4)/81.0

dataset v0 = takeWhile (\(x,y) -> x - 1e-8 < 1) $ iterate (step v' 0.001) (0,v0)

main = writeFile "/tmp/rk.dat" (datasets [-12,-10..0])
  where
    datasets = intercalate "\n" . map (unlines . map showPoint . dataset)
    showPoint (x,y) = (show x) <> " " <> (show y)
