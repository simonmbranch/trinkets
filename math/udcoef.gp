# Trench 5.4 #21
# vim:set ft=gnuplot:
set terminal pngcairo enhanced
set zeroaxis
set output "/tmp/udcoef.png"
set xrange [-2:2]
set yrange [-60:60]
unset key
plot "/tmp/udcoef.dat" using 1:2 with lines
