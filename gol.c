#define _XOPEN_SOURCE 700
#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>

#define START_GLIDER 0
#define RANDOM_CHANCE (1.0/6.0)

/********** Timeval and timespec stuff **********/

/* Copied from OpenBSD src/sys/time.h, see that file for license */

#define	my_timersub(tvp, uvp, vvp)					\
	do {								\
		(vvp)->tv_sec = (tvp)->tv_sec - (uvp)->tv_sec;		\
		(vvp)->tv_usec = (tvp)->tv_usec - (uvp)->tv_usec;	\
		if ((vvp)->tv_usec < 0) {				\
			(vvp)->tv_sec--;				\
			(vvp)->tv_usec += 1000000;			\
		}							\
	} while (0)
#define	my_timeradd(tvp, uvp, vvp)					\
	do {								\
		(vvp)->tv_sec = (tvp)->tv_sec + (uvp)->tv_sec;		\
		(vvp)->tv_usec = (tvp)->tv_usec + (uvp)->tv_usec;	\
		if ((vvp)->tv_usec >= 1000000) {			\
			(vvp)->tv_sec++;				\
			(vvp)->tv_usec -= 1000000;			\
		}							\
	} while (0)
#define	my_timespecsub(tsp, usp, vsp)					\
	do {								\
		(vsp)->tv_sec = (tsp)->tv_sec - (usp)->tv_sec;		\
		(vsp)->tv_nsec = (tsp)->tv_nsec - (usp)->tv_nsec;	\
		if ((vsp)->tv_nsec < 0) {				\
			(vsp)->tv_sec--;				\
			(vsp)->tv_nsec += 1000000000L;			\
		}							\
	} while (0)

/********** Game of Life stuff **********/

int gridheight;
int gridwidth;
bool *grids[2] = { NULL, NULL };
int gridi = 0;
#define GRID(x,y) grids[gridi][(y)*(gridwidth+2) + (x)]
#define OTHERGRID(x,y) grids[ (gridi+1) % 2 ][(y)*(gridwidth+2) + (x)]

#define BENCHMARK_TICKS 20

void simulate(void);

void
newgrid(void)
{
	grids[0] = calloc((gridwidth+2) * (gridheight+2), sizeof(**grids));
	if (!grids[0])
		abort();
	grids[1] = calloc((gridwidth+2) * (gridheight+2), sizeof(**grids));
	if (!grids[1])
		abort();
	gridi = 0;
#if START_GLIDER
	GRID(50,50) = GRID(50,51) = GRID(50,52) = GRID(49,52) = GRID(48,51) = 1;
#else
	for (int x = 1; x <= gridwidth; x++) {
		for (int y = 1; y <= gridheight; y++) {
			GRID(x,y) = drand48() < RANDOM_CHANCE;
		}
	}
#endif
	simulate(); /* run twice just so it's less jarring at the start */
	simulate();
}

void
simulate(void)
{
	struct timespec start, end, elapsed;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);

	for (int x = 1; x <= gridwidth; x++) {
		for (int y = 1; y <= gridheight; y++) {
			int count = GRID(x-1,y-1) + GRID(x-1,y) + GRID(x-1,y+1)
					+ GRID(x,y-1) + GRID(x,y+1)
					+ GRID(x+1,y-1) + GRID(x+1,y) + GRID(x+1,y+1);
			OTHERGRID(x,y) = count == 3 || (GRID(x,y) && count == 2);
		}
	}
	gridi = (gridi+1) % 2;

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	my_timespecsub(&end, &start, &elapsed);

	static long tick = 0;
	static long microseconds_total = 0;
	microseconds_total += elapsed.tv_nsec / 1000;
	if (++tick % BENCHMARK_TICKS == 0) {
		printf("avg us over %d ticks: %ld\n", BENCHMARK_TICKS,
			microseconds_total / BENCHMARK_TICKS);
		microseconds_total = 0;
	}
}

/********** X11 stuff **********/

Display *dpy;
GC gc;
int screen;
Window window;

const int miny = 24;
const int squaresize = 12;

XColor grey;
XColor pink;

void
initcolors(void)
{
	assert(XParseColor(dpy, DefaultColormap(dpy, screen), "#070707", &grey));
	assert(XAllocColor(dpy, DefaultColormap(dpy, screen), &grey));
	assert(XParseColor(dpy, DefaultColormap(dpy, screen), "#FFC0CB", &pink));
	assert(XAllocColor(dpy, DefaultColormap(dpy, screen), &pink));
}

void
resize(void)
{
	XWindowAttributes wattr;
	XGetWindowAttributes(dpy, window, &wattr);
	gridheight = (wattr.height - miny) / squaresize;
	gridwidth = wattr.width / squaresize;
	newgrid();
}

void
expose(void)
{
	XSetWindowBackground(dpy, window, grey.pixel);
	XSetForeground(dpy, gc, pink.pixel);
	XClearWindow(dpy, window);
	for (int x = 1; x <= gridwidth; x++) {
		for (int y = 1; y <= gridheight; y++) {
			if (GRID(x,y)) {
				XFillRectangle(dpy, window, gc,
						x*squaresize, miny + y*squaresize,
						squaresize, squaresize);
			}
		}
	}
	XFlush(dpy);
}

void
loop(void)
{
	int xfd = ConnectionNumber(dpy);
	fd_set selectfds;
	XEvent ev;
	struct timeval period, timeout, nexttick, now;

	period.tv_usec = 100 * 1000;
	period.tv_sec = 0;
	gettimeofday(&now, NULL);
	my_timeradd(&now, &period, &nexttick);
	XSelectInput(dpy, window, StructureNotifyMask | ExposureMask);
	for (;;) {
		FD_ZERO(&selectfds);
		FD_SET(xfd, &selectfds);

		gettimeofday(&now, NULL);
		my_timersub(&nexttick, &now, &timeout);
		int fdcount = select(xfd+1, &selectfds, NULL, NULL, &timeout);
		if (fdcount > 0) {
			/* got an X11 event */
			XNextEvent(dpy, &ev);
			switch (ev.type) {
			case ConfigureNotify:
				resize();
				break;
			case Expose:
				if (ev.xexpose.count == 0) {
					expose();
				}
				break;
			}
		} else if (fdcount == 0) {
			/* timer elapsed */
			simulate();
			expose();
			my_timeradd(&nexttick, &period, &nexttick);
		} else {
			fprintf(stderr, "select error: %s\n", strerror(errno));
			return;
		}
	}
}

int
main(void)
{
	srand48(time(NULL));
	dpy = XOpenDisplay(NULL);
	//XSynchronize(dpy, True);
	if (!dpy)
		errx(1, "unable to open display '%s'", XDisplayName(NULL));
	screen = DefaultScreen(dpy);
	window = RootWindow(dpy, screen);
	initcolors();
	XSetWindowBackground(dpy, window, grey.pixel);
	gc = XCreateGC(dpy, window, 0, NULL);

	resize();
	expose();
	loop();
	XClearWindow(dpy, window);
	XFreeGC(dpy, gc);
	XCloseDisplay(dpy);
}
