#!/usr/bin/env python3
# SPDX-License-Identifier: CC0

from collections import defaultdict
from itertools import takewhile
from math import sqrt
from statistics import median

def primes(keep=False):
    """Generate all prime numbers in sequence."""

    # https://eli.thegreenplace.net/2023/my-favorite-prime-number-generator/
    # https://gist.github.com/scythe/5fb962722934c58c60430180beab86ed
    # https://news.ycombinator.com/item?id=37236099

    q = 2  # the next number being considered
    s = 4  # the smallest square >= q
    r = 2  # square root of s

    # Each prime P which is less than r is a member of the list
    # factors[E], where E is the next multiple of P.
    factors = defaultdict(list)

    while True:
        if q in factors:
            # Composite
            for p in factors[q]:
                factors[p+q].append(p)
            del factors[q]
            if q == s:
                # Square of a composite
                s += 2*r + 1
                r += 1
        elif q == s:
            # Square of a prime
            factors[s+r].append(r)
            s += 2*r + 1
            r += 1
        else:
            # Prime
            yield q
        q += 1

def second_factor_proportion(prime, ps):
    # https://grossack.site/2023/11/08/37-median.html
    # https://news.ycombinator.com/item?id=38242946
    total = 0.0
    for first_factor in ps:
        if first_factor >= prime:
            break
        chance = 1.0 / (first_factor * prime)
        for other_prime in ps:
            if other_prime == first_factor:
                continue
            if other_prime >= prime:
                break
            chance *= 1.0 - 1.0 / other_prime
        total += chance
    return total

def second_factor_proportions(high):
    assert high > 2
    ps = list(takewhile(lambda x: x < high, primes()))
    for p in ps:
        yield p, second_factor_proportion(p, ps)

if __name__ == "__main__":
    total = 0.0
    print("prime   prop        total")
    for prime, prop in second_factor_proportions(100):
        total += prop
        extra = " * median" if total > 0.5 and total-prop < 0.5 else ""
        print("{:5d}   {:.7f}   {:.7f}{}".format(prime, prop, total, extra))
