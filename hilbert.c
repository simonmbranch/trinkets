/* SPDX-License-Identifier: CC0 */

#include <stdio.h>
#include <stdlib.h>

int dx[4] = { 1, 0, -1, 0 };
int dy[4] = { 0, 1, 0, -1 };
void header(int depth);
void footer(void);
void line(int x, int y, int nx, int ny);

void
hilbert(char **stack, int depth)
{
	if (!depth) {
		header(depth);
		footer();
		return;
	}

	int i, x, y, nx, ny, angle;
	i = x = y = angle = 0;

	header(depth);
	stack[i] = "L";
	do {
		switch (*stack[i]++) {
		case '+':
			angle = (angle+1) % 4;
			break;
		case '-':
			angle = (angle+3) % 4;
			break;
		case 'F':
			nx = x + dx[angle];
			ny = y + dy[angle];
			line(x, y, nx, ny);
			x = nx;
			y = ny;
			break;
		case 'L':
			if (i+1 < depth)
				stack[++i] = "+RF-LFL-FR+";
			break;
		case 'R':
			if (i+1 < depth)
				stack[++i] = "-LF+RFR+FL-";
			break;
		case '\0':
			i--;
			break;
		}
	} while (i);
	footer();
}

#define DEPTH 8
int width = 10;
int pad = 5;
int strokewidth = 1;
const char *color = "red";

void header(int depth) {
	int size = (1<<(depth-1))-1;
	printf("<svg version=\"1.1\" width=\"%d\" height=\"%d\">\n",
		size*width+2*pad, size*width+2*pad);
}

void line(int x, int y, int nx, int ny) {
	printf("<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\""
		/* concatenated */ "stroke=\"%s\" stroke-width=\"%d\"/>\n",
		x*width+pad, nx*width+pad,
		y*width+pad, ny*width+pad,
		color, strokewidth);
}

void footer(void) {
	printf("</svg>\n");
}

int main(void) {
	char *stack[DEPTH];
	hilbert(stack, DEPTH);
}
