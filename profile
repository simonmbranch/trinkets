#!/bin/sh
eval "$(ssh-agent)"

export oldpath=$PATH

CVS_RSH=ssh
CVSROOT=anoncvs@anoncvs1.usa.openbsd.org:/cvs
EMAIL='simonmbranch@gmail.com'
NAME='Simon Branch'
PACKAGER="$NAME <$EMAIL>"
PAGER=less
MANPAGER='less -s'
MANPATH=~/trinkets/man:$MANPATH
PATH=~/.local/bin:~/trinkets:~/dwm/bin:~/oman:$PATH
EDITOR=vim
VISUAL=vim
export CVS_RSH CVSROOT EMAIL NAME PACKAGER PAGER MANPAGER MANPATH PATH EDITOR VISUAL ENV

# Delete these vile directories of mysterious origin.
for d in Desktop Documents Music Pictures Public Templates Videos; do
	if [ -e "$HOME/$d" ]; then
		printf 'deleting %s...\n' "$d"
		rmdir "$HOME/$d"
	fi
done

if [ -n "$BASH" ]; then
	. ~/.bashrc
fi
