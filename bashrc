case $- in (*i*);; (*) return;; esac  # for NetBSD ksh using $ENV

stty -ixon
# On OpenBSD STATUS is _POSIX_VDISABLE by default.  Silence errors since
# the name doesn't exist on Linux.
stty status '^T' 2>/dev/null
# ECHOKE needs ECHOK on Linux (n_tty.c) but is not set by default on
# BSD, see termios(4) and system header <sys/ttydefaults.h>.
stty echok

set -o noclobber

alias cp='cp -i' mv='mv -i' ls='LC_COLLATE=C ls -F'

alias gc="git commit" ga="git add"

# Inetutils "hostname -s" sometimes (always?) calls gethostbyname(),
# which blocks for a long time if the internet is slow (e.g., on a
# hotspot).  Shorten the hostname manually.
hostname=$(hostname)
PS1="${hostname%%.*} \$ "  # note: we use a different PS1 in bash

if bc -q </dev/null 2>/dev/null; then
	alias bc="bc -ql"
else
	alias bc="bc -l"
fi

# Reassure ksh that we use emacs bindings even if $VISUAL is vi.
set -o emacs

uq() { awk '!a[$0]
{a[$0]=1}' "$@"; }

if [ -n "$BASH_VERSION" ]; then
	# Stop bash from overriding our ^W binding.
	bind 'set bind-tty-special-chars off'
	bind 'Control-w: backward-kill-word'
	set +o histexpand
	unset HISTFILE
	shopt -s globstar
	PS1='\[\e[38;5;51m\]'"$PS1"'\[\e[0m\]'
fi

if [ -n "$ZSH_VERSION" ]; then
	WORDCHARS=
	setopt nobanghist globstarshort interactivecomments nonotify
	unset HISTFILE
fi

moon
cat ~/doc/motd 2>/dev/null
