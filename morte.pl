% SPDX-License-Identifier: CC0
castle(terrabil).
castle(tintagil).
child(C, M, F) :- daughter(C, M, F).
child(C, M, F) :- son(C, M, F).
conceives(1, 2, igraine, uther).
dies(1, 2, duke_of_tintagil).
duke(duke_of_tintagil).
father(F) :- child(_, _, F).
king(lot).
king(nentres).
king(uriens).
king(uther).
knight(gawaine).
knight(ulfius).
magical(merlin).
magical(morgan).
meets(1, 1, ulfius, merlin).
mother(M) :- child(_, M, _).
person(duke_of_tintagil).
person(elaine).
person(ewain).
person(gawaine).
person(igraine).
person(lot).
person(merlin).
person(morgan).
person(morgawse).
person(nentres).
person(ulfius).
person(uriens).
person(uther).
queen(Q) :- wife(Q, K), king(K).
rules(duke_of_tintagil, terrabil).
rules(duke_of_tintagil, tintagil).
rules(lot, lothian).
rules(lot, orkney).
rules(nentres, garlot).
rules(uriens, gore).
rules(uther, england).
son(ewain, morgan, uriens).
son(gawaine, morgawse, lot).
wages_siege(1, 1, uther, duke_of_tintagil, terrabil).
weds(1, 2, elaine, nentres).
weds(1, 2, igraine, uther).
weds(1, 2, margawse, lot).
weds(1, 2, morgan, uriens).
wife(W, H) :- weds(_, _, W, H).
wife(igraine, duke_of_tintagil).
