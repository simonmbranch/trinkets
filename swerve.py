from sympy import symbols, Matrix
x, vx, vy, w = symbols('x vx vy w', real=True)
# pathplanner order: FL, FR, BL, BR
#                    (1,1), (1,-1), (-1,1), (-1,-1)
r1x = x
r1y = x
r2x = x
r2y = -x
r3x = -x
r3y = x
r4x = -x
r4y = -x
K = Matrix([
    [1, 0, -r1y],
    [0, 1, r1x],
    [1, 0, -r2y],
    [0, 1, r2x],
    [1, 0, -r3y],
    [0, 1, r3x],
    [1, 0, -r4y],
    [0, 1, r4x],
])
for row in K.pinv().tolist():
    print()
    for col in row:
        print(col)
