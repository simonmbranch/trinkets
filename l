#!/usr/bin/perl
use strict;
use warnings;
use feature qw(say signatures);
use integer;
use POSIX qw(strftime);

my @things = qw(amelia program poetry prose frc gopher tennis pushups);
my $outchars = "*+=-";
my $filename = "$ENV{HOME}/doc/l.txt";
my $spacesep = 0;

sub findthing($s) {
	for (my $n = 0; $n <= $#things; $n++) {
		if ($things[$n] =~ /^\Q$s\E/) {
			return $n;
		}
	}
	-1
}

sub rune($s) {
	my $n = findthing($s);
	my $col = 31 + $n%6;
	my $i = $n/6;
	sprintf "\e[%d;1m%s\e[0m", $col, substr($outchars, $i, 1)
}

sub add(@words) {
	my $confused;
	my $day = strftime "%Y-%m-%d", localtime;
	open my $fp, ">>", $filename
		or die "couldn't open '$filename': $!, exiting";
	for my $w (@words) {
		my $i = findthing($w);
		if ($i == -1) {
			print STDERR "$w?  "
				unless $w eq "-";
			$confused = 1;
		} else {
			say $fp "$day $things[$i]";
		}
	}
	if ($confused) {
		say map {rune($_), $_, " "} @things;
	}
}

sub runes() {
	open my $fp, "<", $filename
		or die "couldn't open '$filename': $!, exiting";
	my $prevday;
	my $col = 0;
	my @badthings;
	while (<$fp>) {
		/^(\S+) (\S+)/ or next;
		my $day = $1;
		my $thing = $2;
		if (findthing($thing) == -1) {
			push @badthings, $thing;
			next;
		}
		if (defined($prevday) && $prevday ne $day && $spacesep) {
			print " ";
			$col++;
		}
		$prevday = $day;
		print rune($thing);
		$col++;
		if ($col >= 60) {
			print "\n";
			$col = 0;
		}
	}
	print "\n" if $col;
	if (@badthings) {
		say "ignored ", join(", ", @badthings);
	}
}

add(@ARGV);
runes();
