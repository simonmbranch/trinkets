/* SPDX-License-Identifier: CC0 */
#define _POSIX_C_SOURCE 200809L

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

/* see ascii(7), termios(4), tcsetattr(3) */

const char *repr[] = {
	"^@\tNUL", "^A\tSOH", "^B\tSTX", "^C\tETX", "^D\tEOT", "^E\tENQ",
	"^F\tACK", "^G\tBEL", "^H\tBS",  "^I\tHT",  "^J\tLF",  "^K\tVT",
	"^L\tFF",  "^M\tCR",  "^N\tSO",  "^O\tSI",  "^P\tDLE", "^Q\tDC1",
	"^R\tDC2", "^S\tDC3", "^T\tDC4", "^U\tNAK", "^V\tSYN", "^W\tETB",
	"^X\tCAN", "^Y\tEM",  "^Z\tSUB", "^[\tESC", "^\\\tFS", "^]\tGS",
	"^^\tRS",  "^_\tUS",  "' '\tSPC", [0x7F] = "^?\tDEL"
};

int
main(int argc, char **argv)
{
	int ch, ret, intr, restoretermios, restorescreen;
	struct termios orig, raw;

	ch = ret = intr = restoretermios = restorescreen = 0;
#define error(func) do { perror(func); ret = 1; goto exit; } while (0)

	while ((ch = getopt(argc, argv, "a")) != -1) {
		switch (ch) {
		case 'a':
			printf("\x1B[?1049h");
			restorescreen = 1;
			break;
		default:
			fprintf(stderr, "usage: whwi [-a]\n");
			return 2;
		}
	}

	if (isatty(0)) {
		if (tcgetattr(0, &orig) == -1)
			error("tcgetattr");
		raw = orig;
		raw.c_iflag &= ~(ICRNL|IXON);
		raw.c_lflag &= ~(ECHO|ICANON|IEXTEN|ISIG);
		if (tcsetattr(0, TCSAFLUSH, &raw) == -1)
			error("tcsetattr");
		restoretermios = 1;
		printf("whwi: to exit, press ^C twice\n");
	}

	for (;;) {
		char buf[CHAR_BIT+1];
		char *p = buf;
		unsigned char c;
		errno = 0;
		if (read(0, &c, 1) != 1) {
			if (errno == EINTR)
				continue;
			if (errno)
				error("read");
			goto exit;
		}
		if (c == raw.c_cc[VINTR] || c == raw.c_cc[VQUIT]) {
			if (++intr == 2)
				goto exit;
		} else {
			intr = 0;
		}
		for (unsigned char bit = 0x80; bit; bit >>= 1)
			*p++ = c&bit ? '1' : '0';
		*p = '\0';
		if (c > 0x7F)
			printf("%s\n", buf);
		else if (repr[c])
			printf("%s\t%s\n", buf, repr[c]);
		else if (c == '*') /* so you can segment output */
			printf("%s\t*\t(******)\n", buf);
		else
			printf("%s\t%c\n", buf, c);
	}

exit:
	if (restorescreen)
		printf("\x1B[?1049l");
	if (restoretermios && tcsetattr(0, TCSAFLUSH, &orig) == -1) {
		perror("tcsetattr");
		return 1;
	}
	return ret;
}
