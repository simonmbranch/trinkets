#!/usr/bin/perl
# SPDX-License-Identifier: CC0
# This script is dedicated to the public domain.  Copy it as you please.
use strict;
use warnings;
use feature qw(signatures);

# If files are given on the command line, they are taken as input.
# Otherwise, if stdin is a terminal, the clipboard is the input.
# Otherwise, read text from stdin.
#
# This program counts the uses of each motor and which subsystems it was
# used in.  It prints a table sorted by year and then number of uses.
#
# The input is parsed as follows:
#
# If a line has a colon, it contains a subsystem and some motors used in
# it, for example...
#
#     Swerve azimuth motors: 2024K1, 2024K5, 2023K3, 2023K4.
#
# The subsystems can be any of the keywords listed below in @keywords
# (case insensitive).  The format of a motor is: <year><motor-type><id>,
# where motor-type is a single letter, and year can be e.g. 2024 or 24.
# id is the number of the motor from that year.
#
# Otherwise (the line doesn't have a colon) it should be a header for an
# event.  The script reports the line as "weird" if it doesn't contain a
# month name.  This is so that I don't miss parsing mistakes.

my @keywords = qw(azimuth drive intake rollers flywheel);

if (@ARGV == 0 && -t) {  # no files given and stdin is terminal?
	open STDIN, "xsel -bo|";
}

my %counts;  # motor -> number of occurrences
my %uses;    # motor -> array of subsystems

while (<>) {
	chomp;
	$_ = lc;
	if (/(.*):(.*)/) {  # line with motors?
		my $keys = $1;
		my $motors = $2;
		my @found;
		for my $k (@keywords) {
			if ($keys =~ $k) {
				push @found, $k;
			}
		}
		while ($motors =~ /\b\d+\w\d+\b/g) {
			my $motorname = $&;
			$motorname =~ s/^20//;  # normalize 2024k1 to 24k1
			$counts{$motorname}++;
			push @{$uses{$motorname}}, @found;
		}
	} else {
		# other lines should be empty or include a month
		next if /^\s*$/;
		next if /jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec/;
		print STDERR "weird line: $_\n";
	}
}

sub bad($motor) {
	# create key for sorting motors; worse motors get more negative scores
	$motor =~ /^\d+/;
	my $year = $&;
	return -1000*$year - $counts{$motor};
}

my @motorsworsttobest = sort { bad($a) <=> bad($b) } keys %counts;

print "motor    uses    systems\n";
print "========================\n";
for my $m (@motorsworsttobest) {
	printf "%-9s%-8d%s\n", $m, $counts{$m}, join(", ", @{$uses{$m}});
}
