dofile("irks.secrets")

function oftc()
  flow = {
    {"This nickname is registered and protected", "IDENTIFY %s", oftcpassword}
  }
  dial("irc.oftc.net", 6667)
end

function libera()
  flow = {
    -- To create a SASL key, use something like:
    -- printf '%s\0%s\0%s' nick nick password | base64
    --
    -- https://datatracker.ietf.org/doc/html/rfc4616
    -- https://datatracker.ietf.org/doc/html/rfc4648
    {"This nickname is registered", "CAP REQ :sasl"},
    {"ACK :sasl", "AUTHENTICATE PLAIN"},
    {"AUTHENTICATE +", "AUTHENTICATE %s", liberasasl},
    {"900", "CAP END"}
  }
  dial("irc.libera.chat", 6667)
end

pos = 1

function server(s)
  local auto = false
  if flow[pos] and string.find(s, flow[pos][1]) then
    sendline(string.format(table.unpack(flow[pos], 2)))
    pos = pos + 1
    auto = true
  end

  do
    local found, _, text = string.find(s, "PING.-(:.*)")
    if found then
      sendline("PONG " .. text)
      auto = true
    end
  end

  print((auto and "srv +\t" or "srv\t") .. s)
end

function user(s)
  print("usr *\t" .. s)
  sendline(s)
end

do
  io.write("what server? ")
  io.flush()
  local server = io.read()
  if server == "libera" then
    libera()
  elseif server == "oftc" then
    oftc()
  end
end
sendline("NICK "..nick)
sendline(string.format("USER %s 0 * %s", nick, nick))
timeout = 5
print(loop(user, server))
