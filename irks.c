/* SPDX-License-Identifier: CC0 */

#define _POSIX_C_SOURCE 200809L

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <poll.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

char Host[64];
char Port[64];
FILE *Srv;
struct pollfd Polls[2] = {{.fd = 0, .events = POLLIN}, {.fd = -1, .events = POLLIN}};

int
Ldial(lua_State *L)
{
	snprintf(Host, sizeof Host, "%s", luaL_checkstring(L, 1));
	snprintf(Port, sizeof Port, "%lld", luaL_optinteger(L, 2, 6667));

	int ret;
	struct addrinfo *res;
	struct addrinfo hints = { .ai_family = AF_UNSPEC, .ai_socktype = SOCK_STREAM };
	if ((ret = getaddrinfo(Host, Port, &hints, &res)) != 0)
		return luaL_error(L, "resolving '%s:%s': %s",
				Host, Port, gai_strerror(ret));
	int fd = -1;
	for (struct addrinfo *ai = res; ai; ai = ai->ai_next) {
		if ((fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol)) == -1)
			continue;
		if (connect(fd, ai->ai_addr, ai->ai_addrlen) == 0)
			break;
		close(fd);
	}
	freeaddrinfo(res);
	if (fd == -1)
		return luaL_error(L, "connecting to '%s:%s': %s",
				Host, Port, strerror(errno));
	Srv = fdopen(fd, "r+");
	setbuf(Srv, NULL);
	Polls[1].fd = fd;
	return 0;
}

int
Lsendline(lua_State *L)
{
	size_t len;
	const char *buf = luaL_checklstring(L, 1, &len);
	fwrite(buf, 1, len, Srv);
	fputs("\r\n", Srv);
	if (ferror(Srv))
		luaL_error(L, "cannot write to server: %s",
				strerror(errno));
	return 0;
}

bool
readlinetolua(lua_State *L, FILE *fp, char *fpname)
{
	static char *buf = NULL;
	static size_t size = 0;
	static ssize_t len;

	if ((len = getline(&buf, &size, fp)) == -1) {
		if (ferror(fp))
			return luaL_error(L, "cannot read from %s: %s", fpname, strerror(errno));
		else
			return true;
	}
	if (len > 0 && buf[len-1] == '\n')
		len--;
	if (len > 0 && buf[len-1] == '\r')
		len--;
	lua_pushlstring(L, buf, len);
	lua_call(L, 1, 0);
	return false;
}

int
Lloop(lua_State *L)
{
	luaL_checktype(L, 1, LUA_TFUNCTION);  /* handler for user input */
	luaL_checktype(L, 2, LUA_TFUNCTION);  /* handler for server */
	int timeout = luaL_optinteger(L, 3, 300);

	if (Polls[1].fd == -1)
		return luaL_error(L, "did not dial()");

	bool wantpong = false;
	for (;;) {
		int ret = poll(Polls, 2, timeout*1000);
		if (ret < 0) {
			if (errno == EINTR || errno == EAGAIN)
				continue;
			/* all other poll(2) errors are our fault */
			return luaL_error(L, "irks internal error: poll: %s",
					strerror(errno));
		}
		if (ret == 0) {
			if (wantpong)
				return luaL_error(L, "no PONG from server after %d seconds", timeout);
			fprintf(Srv, "PING %s\r\n", Host);
			wantpong = true;
			continue;
		}
		wantpong = false;
		if ((Polls[0].revents | Polls[1].revents) & (POLLERR|POLLNVAL))
			return luaL_error(L, "polling error");
		if (Polls[0].revents & (POLLIN|POLLHUP)) {
			lua_pushvalue(L, 1);
			if (readlinetolua(L, stdin, "stdin")) {
				lua_pushinteger(L, 0);
				return 1;
			}
		}
		if (Polls[1].revents & (POLLIN|POLLHUP)) {
			lua_pushvalue(L, 2);
			if (readlinetolua(L, Srv, "server")) {
				lua_pushinteger(L, 1);
				return 1;
			}
		}
	}
}

int
main(int argc, char **argv)
{
	const char *conffile = argc>1 ? argv[1] : "irks.lua";

	setbuf(stdout, NULL);
	setbuf(stdin, NULL);

	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	lua_register(L, "dial", Ldial);
	lua_register(L, "sendline", Lsendline);
	lua_register(L, "loop", Lloop);
	if (luaL_dofile(L, conffile))
		fprintf(stderr, "irks: %s\n", lua_tostring(L, -1));
	lua_close(L);
}
