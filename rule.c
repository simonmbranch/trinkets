/* SPDX-License-Identifier: CC0 */

#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

const char *usage = "usage: rule [-brs] [rule [count [width]]]\n";

static int
arg(int argc, char **argv, int i, int def)
{
	if (argc <= i)
		return def;
	int n = atoi(argv[i]);
	return n ? abs(n) : def;
}

static bool
lives(int rule, bool *arr)
{
	return rule & (1 << (4*arr[0] + 2*arr[1] + arr[2]));
}

int
main(int argc, char **argv)
{
	bool dorandom = 0;
	bool dobitmap = 0;
	int dosleep = 0;
	int ch;
	while ((ch = getopt(argc, argv, "brs")) != -1) {
		switch (ch) {
		case 'b':
			dobitmap = 1;
			break;
		case 'r':
			dorandom = 1;
			break;
		case 's':
			dosleep = 1;
			break;
		default:
			fprintf(stderr, usage);
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	int rule = arg(argc, argv, 0, 30);
	int rows = arg(argc, argv, 1, 32);
	int cols = arg(argc, argv, 2, rows*2-1);

	bool *cells = calloc(cols+2, sizeof(*cells));
	bool *nextcells = calloc(cols+2, sizeof(*nextcells));
	if (!cells || !nextcells) {
		fprintf(stderr, "no memory?\n");
		return 2;
	}

	cells[1+cols/2] = 1;
	char *outputchars = ".O";

	if (dorandom) {
		srand((unsigned int)time(NULL));
		for (int i = 1; i <= cols; i++)
			cells[i] = rand() & 1;
	}

	if (dobitmap) {
		printf("P1 %d %d\n", cols, rows);
		outputchars = "01";
	}

	while (dosleep || rows-- > 0) {
		for (int i = 1; i <= cols; i++) {
			putchar(outputchars[cells[i]]);
			nextcells[i] = lives(rule, &cells[i-1]);
		}
		putchar('\n');

		bool *tmpcells = cells;
		cells = nextcells;
		nextcells = tmpcells;
		if (dosleep)
			sleep(1);
	}
}
