\ SPDX-License-Identifier: CC0

\ http://depts.washington.edu/uwcbells/
\ https://www.ringing.info/beginners/ringing.htm
\ https://gutenberg.org/files/18567/18567-h/18567-h.htm

\ The tenor is the largest bell, the treble the smallest.

VARIABLE #bells
VARIABLE #changes

: bellconstant   CREATE ,  DOES>  @ #bells ! ;
3 bellconstant singles   8  bellconstant major
4 bellconstant minimus   9  bellconstant caters
5 bellconstant doubles   10 bellconstant royal
6 bellconstant minor     11 bellconstant cinques
7 bellconstant triples   12 bellconstant maximus

: bell@    PAD + C@ ;
: bell!    PAD + C! ;

: bells.   #bells @ 0 ?DO I bell@ . LOOP  1 #changes +! ;

\ ROUNDS restores the starting position, ROUNDS? checks if we are at
\ rounds.
: rounds   #bells @ 0 ?DO I 1+ I bell! LOOP ;
: rounds?  #bells @ 0 ?DO I bell@ I 1+ <> IF UNLOOP FALSE EXIT THEN LOOP TRUE ;

\ Round up or down to an even number.
: even+    DUP 2 MOD + ;
: even-    DUP 2 MOD - ;

: swap2    2DUP  bell@ SWAP bell@  ROT bell!  SWAP bell! ;
: swapn    ?DO I I 1+ swap2 2 +LOOP ;
: swapall  #bells @ even-  0 swapn ;
: swapint  #bells @ even+  1- 1 swapn ;
: swapbob  #bells @ even-  2 swapn ;

: plainhunt
    0 #changes !
    rounds
    #bells @ 0 ?DO
        CR bells. swapall
        CR bells. swapint
    LOOP
    CR bells. ;

: plainbob
    0 #changes !
    rounds
    #bells @ DUP 1- * 0 ?DO
        CR bells. swapall
        CR bells. 0 bell@ 1 = IF swapbob ELSE swapint THEN
    LOOP
    CR bells. ;
