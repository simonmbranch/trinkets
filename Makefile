# SPDX-License-Identifier: CC0

# Each output file has a Makefile entry, is part of the clean target and
# an all-* target, and is in the .gitignore.

#CC	= musl-gcc -static -Os
CC	= gcc
CFLAGS	= -std=c99 -pedantic -Wall -Wextra -ftrapv -ggdb
SML	= polyc
UXNASM	= uxnasm

include config.mk

all-c: ascii gol hilbert irks jot se/wordcount rule unbin whwi 750ms
all-go: bunco nyt
all-haskell: se/alphanum
all-mandoc: man/mandoc.db
all-uxn: puzzle1.rom

clean:
	rm -f ascii bunco gol hilbert irks man/mandoc.db nyt jot puzzle1.rom puzzle1.rom.sym se/alphanum se/alphanum.hi se/alphanum.o se/wordcount rule unbin whwi 750ms.c

ascii: ascii.c
hilbert: hilbert.c
se/wordcount: se/wordcount.c
rule: rule.c
unbin: unbin.c
whwi: whwi.c
750ms: 750ms.c

bunco: bunco.go
	go build bunco.go

gol: gol.c
	${CC} ${CFLAGS} -lX11 -O3 -o gol gol.c

irks: irks.c
	${CC} ${CFLAGS} -llua -o irks irks.c

jot: jot.c
	${CC} -Os -lm -o jot jot.c

man/mandoc.db: man/man[1-8]/*.[1-8]
	makewhatis man

nyt: nyt.go
	go build nyt.go

puzzle1.rom: puzzle1.tal
	${UXNASM} puzzle1.tal puzzle1.rom

se/alphanum: se/alphanum.hs
	ghc -dynamic se/alphanum.hs

.PHONY: all-c all-mandoc all-sml all-uxn clean
