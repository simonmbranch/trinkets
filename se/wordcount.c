/* SPDX-License-Identifier: CC0 */

/* https://codereview.stackexchange.com/questions/289401
 *
 * Results agree with the classic McIlroy code:
 *
 *     tr -cs A-Za-z '\n' | tr A-Z a-z | sort | uniq -c | sort -rn
 *
 * (but in the output format of the question).
 */

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define WORDCOUNT (1<<16)
#define WORDLEN 64

struct bucket {
	char *word;
	size_t count;
};
struct bucket table[WORDCOUNT] = {0};
size_t nwords = 0;

size_t
hash(char *s)
{
	size_t h = 5381;
	for (unsigned char c; (c = *s++);) {
		h = h*33 ^ c;
	}
	return h;
}

int
process(char *s)
{
	size_t i = hash(s) % WORDCOUNT;
	size_t save = i;
	while (i %= WORDCOUNT, table[i].word && strcmp(table[i].word, s)) {
		i++;
		if (i == save)
			return 1;
	}
	if (!table[i].word) {
		char *ns = strdup(s);
		if (!ns)
			return 1;	/* abort on OOM */
		table[i].word = ns;
		nwords++;
	}
	table[i].count++;
	return 0;
}

void
readinput(FILE *fp)
{
	char wbuf[WORDLEN+1];
	unsigned int wlen = 0;
	int c;
	while ((c = fgetc(fp)) != EOF) {
		if ('A' <= c && c <= 'Z')
			c = c - 'A' + 'a';
		if (c < 'a' || c > 'z') {	/* not a letter, process */
			if (wlen) {
				wbuf[wlen] = '\0';
				if (process(wbuf))
					break;
				wlen = 0;
			}
		} else if (wlen < WORDLEN) {
			wbuf[wlen++] = c;
		}
	}
	wbuf[wlen] = '\0';
	process(wbuf);
}

size_t
compact(void)
{
	size_t slow = 0;
	size_t fast = 0;
	for (;;) {
		while (!table[fast].word && fast < WORDCOUNT)
			fast++;
		if (fast == WORDCOUNT)
			break;
		table[slow++] = table[fast++];
	}
	return slow;
}

int
bucketcmp(const void *a, const void *b)
{
	const struct bucket *ba = a;
	const struct bucket *bb = b;
	return ba->count < bb->count ? -1
			: ba->count > bb->count ? 1
			: strcmp(ba->word, bb->word);
}

int
main(void)
{
	readinput(stdin);
	size_t nmemb = compact();
	qsort(table, nmemb, sizeof(*table), bucketcmp);
	for (size_t i = 0; i < nmemb; i++)
		printf("%s %zu\n", table[i].word, table[i].count);
}
