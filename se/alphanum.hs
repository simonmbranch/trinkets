-- SPDX-License-Identifier: CC0

-- https://math.stackexchange.com/questions/4792965/stumped-on-an-alphanumeric-puzzle

import Data.List (permutations)

main = print . filter good . permutations $ [1..6]
  where
    tens = iterate (10*) 1
    digits = sum . zipWith (*) tens . reverse
    good [a,b,c,d,e,f] = d * digits [b,c,e,a] == digits [f,8,b,0]
